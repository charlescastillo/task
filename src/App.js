import React, { Component } from 'react'
import styled, { ThemeProvider } from 'styled-components'
import { Theme, GlobalStyle } from './theme'
import Header from './screens/header'
import Footer from './screens/footer'
import Main from './screens/main'

export default class App extends Component {
  constructor(props) {
    super(props)
    document.title = 'Wuzap'
  }

  render() {
    return (
      <React.Fragment>
        <GlobalStyle />
        <ThemeProvider className="App" theme={Theme}>
          <Container>
            <Header />
            <Main />
            <Footer />
          </Container>
        </ThemeProvider>
      </React.Fragment>
    )
  }
}

const Container = styled.div`
  height: 100vh;
  width: 100%;
  display: flex;
  flex: 1;
  flex-direction: column;
`
