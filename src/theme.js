import { createGlobalStyle } from 'styled-components'

//fonts
import medium from './res/fonts/HelveticaNeue-Light.otf'

export const Theme = {
  primary: 'rgb(104, 145, 162)',
  darkBG: '#d6d6d6',

  borderColor: 'rgba(0, 0, 0, 0.12)',
  borderColorLight: 'rgba(0, 0, 0, 0.08)',
}

export const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: Medium;
    src: url('${medium}') format('opentype');
  }

  * {
    font-family: 'Medium', sans-serif;
    font-size: ${Theme.fontsm};
    color: ${Theme.fontPrimary};
    margin: 0;
    font-weight:300;
  }

  body{
    margin:0;
    padding:0;
  }

  html, body, #root {
    box-sizing: border-box;
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
  }

  b {
    font-weight:700;
  }

  /* width */
  ::-webkit-scrollbar {
      width: 7px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    background: ${Theme.borderColorLight}; 
    
  }
  
  /* Handle */
  ::-webkit-scrollbar-thumb {
      background: ${Theme.borderColor}; 
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
      background: ${Theme.borderColor}; 
  }
  ::-webkit-clear-button
  {
      display: none; /* Hide the button */
      -webkit-appearance: none; /* turn off default browser styling */
  }
`
