import React from 'react'
import styled from 'styled-components'

const FooterComp = props => {
  return <Footer>&#9400; 2019 by wuzap.de</Footer>
}

const Footer = styled.footer`
  background: ${p => p.theme.primary};
  padding: 10px;
  flex-basis: 20px;
  flex-grow: 0;
  color: #bbb;
`

export default FooterComp
