import React from 'react'
import styled from 'styled-components'

const Message = ({ text, left }) => {
  return <Container left={left}>{text}</Container>
}

const Container = styled.div`
  height: 100%;
  flex: 1;
  max-width: 270px;
  padding: 10px;
  border-radius: 5px;
  background: ${p => p.theme.darkBG};
  margin-bottom: 10px;
  text-align: left;
  ${p => !p.left && `align-self: flex-end;`}
`
export default Message
