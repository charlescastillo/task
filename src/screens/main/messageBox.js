import React from 'react'
import styled from 'styled-components'
import Message from './message'

const MessageBox = props => {
  return (
    <Container>
      {props.messages.map((d, i) => (
        <Message text={d} key={i} left={i % 2 === 0} />
      ))}
    </Container>
  )
}

const Container = styled.article`
  background: white;
  border: 1px gray solid;
  padding: 30px;
  border-radius: 5px 5px 0px 0px;
  max-width: 600px;
  margin: 0 auto;
  text-align: center;
  width: 100%;
  display: flex;
  flex-direction: column;
  flex: 1 1 auto;
  max-height: calc(100% - 180px);
  overflow: auto;
  min-height: 100px;
`
export default MessageBox
