import React, { Component } from 'react'
import styled from 'styled-components'
import axios from 'axios'

export default class extends Component {
  constructor(props) {
    super(props)
    this.state = {
      message: '',
      sending: false,
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (
      nextState.message === this.state.message &&
      nextState.sending === this.state.sending
    )
      return false
    return true
  }

  sendMessage = async () => {
    const { message } = this.state
    if (!message || message === '') return alert('Invalid Text.')
    const res = await axios
      .post('http://bewerbung.wuzap.de/message', {
        message,
      })
      .catch(err => {
        this.setState({ sending: false })
        alert('Unable to send.')
      })
    if (res) {
      this.props.messageSent(message)
      this.setState({ message: '', sending: false })
    }
  }

  onMessageClicked = () => {
    if (this.state.sending) return alert('Please wait...')
    this.setState({ sending: true }, () => this.sendMessage())
  }

  render() {
    return (
      <Container>
        <TextBox
          onChange={e => this.setState({ message: e.target.value })}
          value={this.state.message}
          disabled={this.state.sending}
        />
        <Button onClick={this.onMessageClicked}>
          {this.state.sending ? 'Sending..' : 'Send'}
        </Button>
      </Container>
    )
  }
}

const Container = styled.div`
  flex: 1;
  background: #bac3c4;
  max-width: 662px;
  margin: 0 auto;
  flex-direction: row;
  display: flex;
`

const TextBox = styled.input`
  &:focus {
    outline: none;
  }
  ${p => p.disabled && `color:grey;`}
  padding: 5px;
  flex: 1;
  font-size: 18px;
  border-top: 0px;
  border-bottom: 1px solid gray;
  border-left: 1px solid gray;
  border-right: 1px solid gray;
  border-radius: 0px 0px 0px 3px;
`

const Button = styled.div`
  padding: 5px;
  background: #9a9a9a;
  flex: 1;
  flex-grow: 0;
  flex-basis: 70px;
  border-radius: 0px 0px 3px 0px;
  text-align: center;
  cursor: pointer;
  border-top: 0px;
  border-left: 0px;
  border-right: 1px solid gray;
  border-bottom: 1px solid gray;
  color: white;
`
