import React, { Component } from 'react'
import styled from 'styled-components'
import axios from 'axios'

import MessageBox from './messageBox'
import InputBox from './input'

export default class extends Component {
  constructor(props) {
    super(props)
    this.state = {
      messages: [],
    }
    this.messageTimer = null
  }

  componentDidMount() {
    this.initGetMessages()
  }

  initGetMessages = () => {
    if (this.messageTimer) clearTimeout(this.messageTimer)
    this.getMessage()
    this.messageTimer = setInterval(() => this.getMessage(), 30000)
  }

  getMessage = () => {
    axios
      .get('http://bewerbung.wuzap.de/message', {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then(response => {
        const { Message: newMessage } = response.data
        const { messages } = this.state
        messages.push(newMessage)
        this.setState({ messages })
      })
  }

  messageSent = newMessage => {
    const { messages } = this.state
    messages.push(newMessage)
    this.setState({ messages })
  }

  render() {
    return (
      <Container>
        <MessageBox messages={this.state.messages} />
        <InputBox messageSent={this.messageSent} />
      </Container>
    )
  }
}

const Container = styled.article`
  height: 100%;
  flex: 1;
  box-sizing: border-box;
  padding: 50px;
  background: #bac3c4;
`
