import React from 'react'
import styled from 'styled-components'

const ReactHeader = props => {
  return (
    <Header>
      <Img src="http://bewerbung.wuzap.de/static/images/logo.png" />
    </Header>
  )
}

const Header = styled.div`
  flex-basis: 55px;
  flex-grow:0
  background: ${p => p.theme.primary};
`
const Img = styled.img`
  padding: 10px;
`

export default ReactHeader
